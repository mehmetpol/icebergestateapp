﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="haritadagosterv2.aspx.cs" Inherits="RouteMobile.haritadagosterv2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Waypoints in Directions</title>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDn8LevE77hl2SSfiumASWXslj-lbT44OU"></script> 
    <style>
        #right-panel {
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

            #right-panel select, #right-panel input {
                font-size: 15px;
            }

            #right-panel select {
                width: 100%;
            }

            #right-panel i {
                font-size: 12px;
            }

        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #map {
            height: 100%;
            float: left;
            width: 70%;
            height: 100%;
        }

        #right-panel {
            margin: 20px;
            border-width: 2px;
            width: 20%;
            height: 400px;
            float: left;
            text-align: left;
            padding-top: 0;
        }

        #directions-panel {
            margin-top: 10px;
            background-color: #FFEE77;
            padding: 10px;
            overflow: scroll;
            height: 174px;
        }

        table.blueTable {
  border: 1px solid #1C6EA4;
  background-color: #EEEEEE;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  border: 1px solid #AAAAAA;
  padding: 3px 2px;
}
table.blueTable tbody td {
  font-size: 13px;
}
table.blueTable tr:nth-child(even) {
  background: #D0E4F5;
}
table.blueTable thead {
  background: #1C6EA4;
  background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  border-bottom: 2px solid #444444;
}
table.blueTable thead th {
  font-size: 15px;
  font-weight: bold;
  color: #FFFFFF;
  border-left: 2px solid #D0E4F5;
}
table.blueTable thead th:first-child {
  border-left: none;
}

table.blueTable tfoot {
  font-size: 14px;
  font-weight: bold;
  color: #FFFFFF;
  background: #D0E4F5;
  background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  border-top: 2px solid #444444;
}
table.blueTable tfoot td {
  font-size: 14px;
}
table.blueTable tfoot .links {
  text-align: right;
}
table.blueTable tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}

    </style>      
</head>
<body>
    <div id="map"></div> 
    <div id="right-panel"> 
         <p>
             <asp:HyperLink ID="hlink" runat="server">Yenile</asp:HyperLink>
             
             
             <center>KÜME ÖZETİ</center><br />
             <asp:Repeater ID="rpt_ozet" runat="server">
                 <HeaderTemplate><table style="width:100%" class="blueTable"><tr>
                     <th>Küme No</th>
                     <th>Hacim</th>
                     <th>KG</th>
                      <th>Palet</th>
                     <th>Tutar</th></tr></HeaderTemplate>
                 <ItemTemplate>
                   <tr>
                       <td style="text-align:center"><b><%#Eval("clusterno") %></b></td>
                       <td style="text-align:right"><%#Eval("hacim","{0:N2}") %> m<sup>3</sup></td>
                       <td style="text-align:right"><%#Eval("kg","{0:N2}") %> Kg</td>
                       <td style="text-align:right"><%#Eval("Palet","{0:N2}") %> PLT</td>
                       <td style="text-align:right"><%#Eval("Tutar","{0:N2}") %> TL</td>
                   </tr>
                 </ItemTemplate>
                 <FooterTemplate></table></FooterTemplate>
             </asp:Repeater>
         </p>
 
   <form id="form1" runat="server"> <asp:DropDownList ID="ddl_sf" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_sf_SelectedIndexChanged">
        <asp:ListItem Value="T" Text=" Tümü"></asp:ListItem>         
        <asp:ListItem Value="S" Text="Sipariş"></asp:ListItem>
                 <asp:ListItem Value="F" Text="Fatura"></asp:ListItem>
             </asp:DropDownList>
       <asp:Literal ID="ltr" runat="server"></asp:Literal>
   </form>   </div>
<script type="text/javascript">  
    window.onload =
        function () {
            initMap();
        }
</script>
</body>
</html>
