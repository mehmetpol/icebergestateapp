﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="by_gh_konumkaydetplus.aspx.cs" Inherits="SCBB2B._by_gh_konumkaydetplus" %>
<html><head>
    <title>Konum Kaydet</title>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no,user-scalable=no,viewport-fit=cover">
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDw3iFRNU9J3GsVnTchCV7KiJ6jiP0Cfao"></script>
       <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <link href="css/jquery.autocomplete.css" rel="stylesheet" />
    <script src="js/jquery.autocomplete.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[id$=txt_Cari]').autocomplete('Arama.ashx?t=cr&k=1&pla=<%#Session["Turu"].ToString()=="plasiyer"?Session["ID"].ToString():"0"%>');
        });

        var geocoder = new google.maps.Geocoder();
        var klt = '';
        var klg = '';
        var latLng;
        var latLng_neredeyim;
        var map;
        var marker;
        var marker_neredeyim;

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Adres bulunamadı.');
                }
            });
        }

        function updateMarkerPosition(latLng) {
            document.getElementById('hidLng').value = [latLng.lat(), latLng.lng()].join(', ');
        }

        function updateMarkerAddress(str) {
            document.getElementById('address').innerHTML = str;
            document.getElementById('hidAdres').innerHTML = str;
        }

        function initialize() {
            klt = '<%= Session["klat"] %>';
            klg = '<%= Session["klng"] %>';

            //document.getElementById('blg').innerHTML = ;
            latLng = new google.maps.LatLng(parseFloat(klt.replace(/,/g, "."), 10), parseFloat(klg.replace(/,/g, "."), 10));
            map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 15,
                center: latLng,
                panControl: false,
                zoomControl: false,
                mapTypeControl: true,
                scaleControl: false,
                streetViewControl: false,
                overviewMapControl: false,
                mapTypeIds: [google.maps.MapTypeId.ROADMAP,
              google.maps.MapTypeId.SATELLITE]
            });


            marker = new google.maps.Marker({
                position: latLng,
                title: '<%= Session["haritacari"] %> için haritada konum belirleyiniz...',
                map: map,
                draggable: true
            });

            if ('<%= Session["haritacari"] %>' == "-") {
                document.getElementById("btn_musterikonum").style.visibility = "hidden";
            }

            marker_neredeyim = new google.maps.Marker({
                icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                position: latLng_neredeyim,
                title: 'Bulunduğunuz konum',
                map: map,
                draggable: false
            });

            // Add dragging event listeners.
            google.maps.event.addListener(marker, 'dragstart', function () {
                updateMarkerAddress('Hesaplanıyor...');
            });

            google.maps.event.addListener(marker, 'drag', function () {
                updateMarkerPosition(marker.getPosition());
            });

            google.maps.event.addListener(marker, 'dragend', function () {
                geocodePosition(marker.getPosition());
            });

        }

        function musterininkonumanagit() {

            klt = '<%= Session["klat"] %>';
            klg = '<%= Session["klng"] %>';
            latLng = new google.maps.LatLng(parseFloat(klt.replace(/,/g, "."), 10), parseFloat(klg.replace(/,/g, "."), 10));
            map.setCenter(latLng);
            marker.setPosition(latLng);
        }

        function gercekkonumanagit() {
            // Try HTML5 geolocation
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    latLng_neredeyim = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    map.setCenter(latLng_neredeyim);
                    marker_neredeyim.setPosition(latLng_neredeyim);
                }, function () {
                    handleNoGeolocation(true);
                });
            }
            else {
                map.setCenter(latLng);
            }
        }
        
        function handleNoGeolocation(errorFlag) {
            if (errorFlag) {
                var content = 'Error: The Geolocation service failed.';
            } else {
                var content = 'Error: Your browser doesn\'t support geolocation.';
            }
        }

        // Onload handler to fire off the app.
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <style>
        #mapCanvas
        {
            width: 100%;
            height: 390px;
            float: left;
        }
        #address
        {
            float: right;
            margin-top: 10px;
        }
    </style>
     <style type="text/css">
        .button
        {
            display: inline-block;
            text-decoration: none;
            font: bold 12px/12px HelveticaNeue, Arial;
            padding: 8px 11px;
            color: #555;
            border: 1px solid #dedede;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }
        .button.blue
        {
            background: #92dbf6;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#abe4f8', endColorstr='#6fcef3'); /*  IE */
            background: -webkit-gradient(linear, left top, left bottom, from(#abe4f8), to(#6fcef3)); /*  WebKit */
            background: -moz-linear-gradient(top,  #abe4f8, #6fcef3);
            border-color: #8dc5da #76b7cf #63abc7;
            color: #42788e;
            text-shadow: 0 1px 0 #b6e6f9;
            -webkit-box-shadow: 0 1px 1px #d6d6d6, inset 0 1px 0 #c0ebfa;
            -moz-box-shadow: 0 1px 1px #d6d6d6, inset 0 1px 0 #c0ebfa;
            box-shadow: 0 1px 1px #d6d6d6, inset 0 1px 0 #c0ebfa;
        }
        .button.blue:hover
        {
            background: #92dbf6;
            border-color: #7caec0 #68a3ba #5a9cb5;
            text-shadow: 0 1px 0 #bee9fa;
            -webkit-box-shadow: 0 1px 1px #d6d6d6, inset 0 1px 0 #ade4f8;
            -moz-box-shadow: 0 1px 1px #d6d6d6, inset 0 1px 0 #ade4f8;
            box-shadow: 0 1px 1px #d6d6d6, inset 0 1px 0 #ade4f8;
        }
        
        .button.red
        {
            background: #f67689;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f78297', endColorstr='#f56778'); /* IE */
            background: -webkit-gradient(linear, left top, left bottom, from(#f78297), to(#f56778)); /* WebKit */
            background: -moz-linear-gradient(top,  #f78297, #f56778);
            border-color: #df6f8b #da5f75 #d55061;
            color: #913944;
            text-shadow: 0 1px 0 #f89ca9;
            -webkit-box-shadow: 0 1px 1px #c1c1c1, inset 0 1px 0 #f9a1b1;
            -moz-box-shadow: 0 1px 1px #c1c1c1, inset 0 1px 0 #f9a1b1;
            box-shadow: 0 1px 1px #c1c1c1, inset 0 1px 0 #f9a1b1;
        }
        .button.red:hover
        {
            background: #f67c90;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f56c7e', endColorstr='#f78297'); /* IE */
            background: -webkit-gradient(linear, left top, left bottom, from(#f56c7e), to(#f78297)); /* WebKit */
            background: -moz-linear-gradient(top,  #f56c7e, #f78297);
            border-color: #c36079 #c25669 #c14e5c;
            text-shadow: 0 1px 0 #f9a6b4;
            -webkit-box-shadow: 0 1px 1px #c3c3c3, inset 0 1px 0 #f8909e;
            -moz-box-shadow: 0 1px 1px #c3c3c3, inset 0 1px 0 #f8909e;
            box-shadow: 0 1px 1px #c3c3c3, inset 0 1px 0 #f8909e;
        }
    </style>

</head>
<body style="font:12px HelveticaNeue, Arial;">
        <form id="form1" runat="server">
<table style="width:100%"><tr><td><a href="inside.aspx"  class="button blue">B2B ye Geri Dön</a></td><td style="text-align: center;vertical-align:middle;"><b>KONUM BİLGİSİ KAYDET</b></td></tr></table>

    <table style="width: 100%;margin-bottom:2px;">
        <tr>
            <td style="width:80%">
                <asp:TextBox ID="txt_Cari" Width="100%" Height="30px" runat="server" placeholder="Cari Kodu ve Ünvanı"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_Sec" runat="server" Text="Cari Seç"  CssClass="button blue" OnClick="btn_Sec_Click"/>
            </td>
        </tr>
        <tr>
            <td style="height:40px"  colspan="3">
                <asp:Label ID="lbl_adres" runat="server" Text=""></asp:Label> &nbsp;&nbsp;&nbsp; <asp:Label ID="lbl_sehir" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
    <div id="mapCanvas">
    </div>
    <br />
            <table style="width:100%">
                <tr><td><asp:Button ID="btn_kaydet" runat="server" Text="Konumu Kaydet" Style="float: left"  CssClass="button blue" OnClick="btn_kaydet_Click" /></td>
                    <td>  <input type="button" id="btn_neredeyim" name="btn_neredeyim" value="Ben Neredeyim"
        class="button blue" onclick="gercekkonumanagit();return false;" /></td>
                    <td> <input type="button" id="btn_musterikonum" name="btn_musterikonum" value="Müşteri Nerede"
        class="button red" onclick="musterininkonumanagit();return false;" /></td></tr>
            </table>
    
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;
   
    <div id="address">
    </div>
    <div id="blg">
    </div>
    <input type="hidden" id="hidLat" name="hidLat" runat="server" />
    <input type="hidden" id="hidLng" name="hidLng" runat="server" />
    <input type="hidden" id="hidAdres" name="hidAdres" runat="server" />
</form>
</body>
    </html>
