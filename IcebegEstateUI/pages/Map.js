
                  // calculate distance
    function calculateDistance() {
            var origin = '<%=baslangic%>';
            var destination = '<%=bitis%>';
                
                var service = new google.maps.DistanceMatrixService();
                service.getDistanceMatrix({
        origins: [origin],
                    destinations: [destination],
                    travelMode: google.maps.TravelMode.DRIVING,
                    // unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
                    unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
                    avoidHighways: false,
                    avoidTolls: false
                }, callback);
            }
            // get distance results
            function callback(response, status) {
                if (status != google.maps.DistanceMatrixStatus.OK) {
        $('#result').html(err);
                } else {
                    var origin = response.originAddresses[0];
                    var destination = response.destinationAddresses[0];
                    if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
        $('#result').html("Better get on a plane. There are no roads between " + origin + " and " + destination);
                    } else {
                        var distance = response.rows[0].elements[0].distance;
                        var duration = response.rows[0].elements[0].duration;
                        console.log(response.rows[0].elements[0].distance);
                        var distance_in_kilo = distance.value / 1000; // the kilom
                        var distance_in_mile = distance.value / 1609.34; // the mile
                        var duration_text = duration.text;
                        var duration_value = duration.value;
                        var total_cost = distance_in_kilo * 3.44;
                        var hidden_mesafe = document.getElementById('<%= hdn_mesafe.ClientID%>');
                        var hidden_mesafe2 = document.getElementById('<%= hdnResultValue.ClientID%>');

                        // $('#in_mile').text(distance_in_mile.toFixed(2));
                        $('#in_kilo').text(distance_in_kilo.toFixed(2) + ' km');
                        hidden_mesafe.value = distance_in_kilo.toFixed(2);
                        // $('#duration_text').text(duration_text);
                        // $('#duration_value').text(duration_value);
                        $('#from').text(origin);
                        $('#to').text(destination);
                        $('#cost').text(total_cost.toFixed(2) + ' zł');
                        hidden_mesafe.value = distance_in_kilo.toFixed(2);
                        hidden_mesafe2.value = $('#in_kilo').text;
                    }
                }
            }
            // print results on submit the form
            function save()  {
        //  e.preventDefault();
        calculateDistance();
            };



    $(document).ready(function () {
        $('#<%=txt_PickUpDate.ClientID%>').datepick({
            dateFormat: 'dd.mm.yyyy', firstDay: 1
        });
        });


        var count = "1";

        function addRow(in_tbl_name) {

            var tbody = document.getElementById(in_tbl_name).getElementsByTagName("TBODY")[0];

            // create row

            var row = document.createElement("TR");

            // create table cell 2

            var td1 = document.createElement("TD")

            var strHtml2 = "<input type=\"text\" name=\"ItemDescription\" class=\"form-control m-input\" placeholder=\"Item Description\"  />";

            td1.innerHTML = strHtml2.replace(/!count!/g, count);

            // create table cell 3

            var td2 = document.createElement("TD")

            var strHtml3 = "<div class=\"m-checkbox-list\"><label class=\"m-checkbox m-checkbox--solid m-checkbox--brand\"><input name=\"Vat\" type=\"checkbox\">VAT<span></span></label></div > ";

td2.innerHTML = strHtml3.replace(/!count!/g, count);

// create table cell 4

var td3 = document.createElement("TD")

var strHtml4 = "<input type=\"text\" name=\"Company\" class=\"form-control m-input\" placeholder=\"Company\"  />";

td3.innerHTML = strHtml4.replace(/!count!/g, count);

// create table cell 5

var td4 = document.createElement("TD")

var strHtml5 = "<input type=\"text\" name=\"Driver\" class=\"form-control m-input\" placeholder=\"Driver\"  />";

td4.innerHTML = strHtml5.replace(/!count!/g, count);

// create table cell 6

var td5 = document.createElement("TD")

var strHtml6 = "<button type=\"button\" onclick=\"deleteRow(this.parentNode.parentNode.rowIndex)\" class=\"btn btn-danger\"><i class=\"flaticon-delete-1\"></i>Delete</button>";

td5.innerHTML = strHtml6.replace(/!count!/g, count);

//<button type="button" onclick="deleteRow(this.parentNode.parentNode.rowIndex)" class="btn btn-danger">Delete</button>
// append data to row

row.appendChild(td1);

row.appendChild(td2);

row.appendChild(td3);

row.appendChild(td4);

row.appendChild(td5);

count = parseInt(count) + 1;

// add to count variable

// append row to table

tbody.appendChild(row);

        }

function delRow(e) {
    e = e || window.Event;
    var current = e.srcElement || e.target;

    //here we will delete the line

    while ((current = current.parentElement) && current.tagName != "TR");

    current.parentElement.removeChild(current);

}

function deleteRow(i) { document.getElementById('tblPets').deleteRow(i); }

    
            //var geocoder;
            //var map;
            //var originCoords;
            //var destCoords;
            //var markerOrigin, markerDest;
            //var directionsService;
            //var directionsDisplay;

            //function initialize() {
            //    directionsService = new google.maps.DirectionsService;
            //    directionsDisplay = new google.maps.DirectionsRenderer;
            //    map = new google.maps.Map(
            //        document.getElementById("map_canvas"), {
            //            center: new google.maps.LatLng(37.4419, -122.1419),
            //            zoom: 13,
            //            mapTypeId: google.maps.MapTypeId.ROADMAP
            //        });
            //};
            //Burada lokasyonlari otomatik dolduruyor, ve hidden valuelere atiyoruz burada sorun yok
            $(function () {

                var directionsService = new google.maps.DirectionsService();
                var directionsDisplay = new google.maps.DirectionsRenderer();

                var pickUp;
                var dropoff;
                // add input listeners
                google.maps.event.addDomListener(window, 'load', function () {
                    var from_places = new google.maps.places.Autocomplete(document.getElementById('<%= txt_PickUpLocation.ClientID%>'));
                    var to_places = new google.maps.places.Autocomplete(document.getElementById('<%= txt_DropOffLocation.ClientID%>'));

                    google.maps.event.addListener(from_places, 'place_changed', function () {
                        var from_place = from_places.getPlace();
                        var from_address = from_place.formatted_address;
                        pickUp = from_place.geometry.location;
                        $('#origin').val(from_address);
                        //  document.getElementById("origin").value = from_address;
                        //  $('#origin').value=from_address;
                        var hidden_fromPlaces = document.getElementById('<%= hdn__PickUpLocation.ClientID%>');
                        var hidden_PlatLng = document.getElementById('<%= hdn_PlatLng.ClientID%>');
                        hidden_fromPlaces.value = from_address;
                        hidden_PlatLng.value = pickUp;
                        // document.getElementById("hdn_fromPlaces").value = from_address;
                        var map = new google.maps.Map(document.getElementById('map_canvas'), {
                            zoom: 13,
                            center: pickUp,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        directionsDisplay.setMap(map);
                    });

                    google.maps.event.addListener(to_places, 'place_changed', function () {
                        var to_place = to_places.getPlace();
                        var to_address = to_place.formatted_address;
                        dropoff = to_place.geometry.location;
                        var hidden_to_places = document.getElementById('<%= hdn_DropOffLocation.ClientID%>');
                        var hidden_DlatLng = document.getElementById('<%= hdn_DlatLng.ClientID%>');
                        hidden_to_places.value = to_address;
                        hidden_DlatLng.value = dropoff;
                        //$('#destination').val(to_address);
                        //  $('#destination').value = to_address;
                        // document.getElementById("destination").value = to_address;
                        //document.getElementById("hdn_to_places").value = to_address;

                        var request = {
                            origin: pickUp,
                            destination: dropoff,
                            travelMode: google.maps.DirectionsTravelMode.DRIVING
                        };

                        directionsService.route(request, function (response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                directionsDisplay.setDirections(response);
                            }
                        });
                        //var request = {
                        //    origin: pickUp,
                        //    destination: dropoff,
                        //    travelMode: google.maps.DirectionsTravelMode.DRIVING
                        //};
                    });




                    //directionsDisplay.setPanel(document.getElementById('panel'));

                    //    var pickup = document.getElementById('<%= txt_PickUpLocation.ClientID%>').value;
                    //   var dropoff = document.getElementById('<%= txt_DropOffLocation.ClientID%>').value;

                });

            });



    
    function toggleBoxVisibility() {

        if (document.getElementById("<%=ddl_BookingType.ClientID%>").value == "1") {

            document.getElementById("<%=OneWay.ClientID%>").style.display = "inline";
            document.getElementById("<%=Hourly.ClientID%>").style.display = "none";
        }
        else {

            document.getElementById("<%=OneWay.ClientID%>").style.display = "none";
            document.getElementById("<%=Hourly.ClientID%>").style.display = "inline";
        }
        if (document.getElementById("<%=ddl_BookingType.ClientID%>").value == "2") {
            document.getElementById("<%=OneWay.ClientID%>").style.display = "none";
            document.getElementById("<%=Hourly.ClientID%>").style.display = "inline";

        }
        else {

            document.getElementById("<%=OneWay.ClientID%>").style.display = "inline";
            document.getElementById("<%=Hourly.ClientID%>").style.display = "none";
        }
    }
    
        $(document).ready(function () {
            $('#<%=ddl_BookingType.ClientID%>').change(function () {

                if (document.getElementById("<%=ddl_BookingType.ClientID%>").value == "1") {

                    document.getElementById("<%=OneWay.ClientID%>").style.display = "inline";
                    document.getElementById("<%=Hourly.ClientID%>").style.display = "none";
                }
                else {

                    document.getElementById("<%=OneWay.ClientID%>").style.display = "none";
                    document.getElementById("<%=Hourly.ClientID%>").style.display = "inline";
                }
                if (document.getElementById("<%=ddl_BookingType.ClientID%>").value == "2") {
                    document.getElementById("<%=OneWay.ClientID%>").style.display = "none";
                    document.getElementById("<%=Hourly.ClientID%>").style.display = "inline";

                }
                else {

                    document.getElementById("<%=OneWay.ClientID%>").style.display = "inline";
                    document.getElementById("<%=Hourly.ClientID%>").style.display = "none";
                }
            })
        });
    

        function GetPrice() {
            var BookingType = $('#<%=ddl_BookingType.ClientID%>').val();
            var CarClass = $('#<%=ddl_CarClasses.ClientID%>').val();
            var PickUp = $('#<%=txt_PickUpLocation.ClientID%>').val();
            var DropOff = $('#<%=txt_DropOffLocation.ClientID%>').val();
            var Hour = $('#<%=ddl_Hours.ClientID%>').val();
            var Agent_ID = '0';
           // alert("GetPrice");
             if ((!BookingType || BookingType == 1)) {
                    //Oneway Diger alanlari kontrol et, dolu mu

                // alert("BookingType");
                    if ((!CarClass || CarClass != 0)) {
                        //CarClass Var
                        // alert("CarClass");
                        if ((PickUp.length != 0)) {
            console.log(PickUp.length);
                           // alert("PickUp");

                            //PicUp Var
                            if ((DropOff.length != 0)) {
            console.log(DropOff.length);
                               // alert("DropOff");
                                //Dropoff Var
                                //Ajax Cagir
                                   $.ajax({
            type: "POST",
                                       url: "Booking_New.aspx/CalculatePrice",
                                       data: JSON.stringify({BookType: '1', CarClass_ID: CarClass, PickUpLoc: PickUp, DropOffLoc: DropOff, AgentID: Agent_ID }),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (doc) {
                                        // alert("Success");
                                        var events = [];
                                      //  console.log(doc.d);
                                        var obj = $.parseJSON(doc.d);
                                        console.log(obj);
                                       //
                                     // $(obj).each(function () {


                                        //$(obj).each(function () {
                                        //    events.push({
                                        //        Value: $(this).attr('Value'),
                                        //        TotalTime: $(this).attr('TotalTime'),
                                        //        Price: $(this).attr('Price'),
                                        //        CalcMethod: $(this).attr('CalcMethod'),
                                        //    });
                                        //});


                                        var myJSON = JSON.stringify(obj);
                                        myJSON = myJSON.replace("[", "");
                                        myJSON = myJSON.replace("]", "");
                                     //   var myObj = {"Value":"39","TotalTime":"42","Price":351,"CalcMethod":"Luxury Class-> <strong>351</strong> Computed as 351 = 0 + ( 39ml * 9 )"};
                                        var myObj = JSON.parse(myJSON);
                                        var theKey = "Price";
                                        console.log(myJSON);
                                        console.log(myObj);
                                        document.getElementById("showPrice").innerHTML = myObj.Price;
                                        document.getElementById("<%=txt_Price.ClientID%>").value = myObj.Price;
                                        document.getElementById("<%=hdn_duration.ClientID%>").value = myObj.TotalTime;
                                        document.getElementById("<%=hdn_Mile.ClientID%>").value = myObj.Value;
                                        document.getElementById("calcMethod").innerHTML = myObj.CalcMethod;

                                       // });
                                       // callback(events);
                                        doc.d
                                      //  console.log(events);

                                    },
                                    error: function (xhr, status, error) {
            alert(xhr.responseText);
                                    }
                                });
                            }
                        }
                    }

                }
                else if ((!BookingType || BookingType == 2)) {
                    //Hourly Diger alanlari kontrol et, dolu mu
                    if ((!CarClass || CarClass != 0)) {
                        //CarClass Var
                        if ((!PickUp || PickUp.length != 0)) {
                            //PicUp Var
                            if ((!Hour || Hour != 0)) {
            //Saat Var
            //Ajax Cagir
            $.ajax({
                type: "POST",
                url: "Booking_New.aspx/CalculatePrice",
                data: JSON.stringify({ BookType: '2', CarClass_ID: CarClass, PickUpLoc: PickUp, DropOffLoc: Hour, AgentID: Agent_ID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (doc) {
                    // alert("Success");
                    var events = [];
                    console.log(doc.d);
                    var obj = $.parseJSON(doc.d);
                    console.log(obj);

                    var myJSON = JSON.stringify(obj);
                    myJSON = myJSON.replace("[", "");
                    myJSON = myJSON.replace("]", "");
                    //   var myObj = {"Value":"39","TotalTime":"42","Price":351,"CalcMethod":"Luxury Class-> <strong>351</strong> Computed as 351 = 0 + ( 39ml * 9 )"};
                    var myObj = JSON.parse(myJSON);
                    var theKey = "Price";
                    console.log(myJSON);
                    console.log(myObj);
                    document.getElementById("showPrice").innerHTML = myObj.Price;
                    document.getElementById("<%=txt_Price.ClientID%>").value = myObj.Price;
                    document.getElementById("<%=hdn_duration.ClientID%>").value = myObj.TotalTime;
                    document.getElementById("<%=hdn_Mile.ClientID%>").value = myObj.Value;
                    document.getElementById("calcMethod").innerHTML = myObj.CalcMethod;
                    doc.d
                    console.log(events);

                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
                            }
                        }
                    }
                }


        }

    