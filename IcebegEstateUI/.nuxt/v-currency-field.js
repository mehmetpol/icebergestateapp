import Vue from 'vue';
import VCurrencyField from 'v-currency-field';

Vue.use(VCurrencyField, {"locale":"tr-TR","decimalLength":0,"autoDecimalMode":false,"min":null,"max":null,"defaultValue":null,"valueAsInteger":false,"allowNegative":false,"distractionFree":false});
