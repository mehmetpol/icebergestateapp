import Vue from 'vue'
import Vuex from 'vuex'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from '..\\layouts\\error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_bootstrapvue_b081ad28 from 'nuxt_plugin_bootstrapvue_b081ad28' // Source: .\\bootstrap-vue.js (mode: 'all')
import nuxt_plugin_vcurrencyfield_4fcff766 from 'nuxt_plugin_vcurrencyfield_4fcff766' // Source: .\\v-currency-field.js (mode: 'all')
import nuxt_plugin_nuxtgooglemaps_5ab8847a from 'nuxt_plugin_nuxtgooglemaps_5ab8847a' // Source: .\\nuxt-google-maps.js (mode: 'all')
import nuxt_plugin_VueResource_4bf21b44 from 'nuxt_plugin_VueResource_4bf21b44' // Source: ..\\plugins\\VueResource (mode: 'all')
import nuxt_plugin_VueMoment_21420450 from 'nuxt_plugin_VueMoment_21420450' // Source: ..\\plugins\\VueMoment (mode: 'all')
import nuxt_plugin_localStorage_830ec59e from 'nuxt_plugin_localStorage_830ec59e' // Source: ..\\plugins\\localStorage.js (mode: 'client')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"layout","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

const originalRegisterModule = Vuex.Store.prototype.registerModule
const baseStoreOptions = { preserveState: process.client }

function registerModule (path, rawModule, options = {}) {
  return originalRegisterModule.call(this, path, rawModule, { ...baseStoreOptions, ...options })
}

async function createApp(ssrContext, config = {}) {
  const router = await createRouter(ssrContext)

  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router

  // Fix SSR caveat https://github.com/nuxt/nuxt.js/issues/3757#issuecomment-414689141
  store.registerModule = registerModule

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"title":"Iceberg Estate","meta":[{"charset":"utf-8"},{"lang":"en"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"description","name":"description","content":"Iceber Estate"}],"htmlAttrs":{"lang":"tr"},"link":[{"rel":"icon","type":"image\u002Fpng","href":"\u002Fimg\u002Ffav.png"},{"rel":"stylesheet","href":"\u002Fcss\u002Fuygulama.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fcss\u002FScrollBar.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002Ffontawesome-free\u002Fcss\u002Fall.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"https:\u002F\u002Fcode.ionicframework.com\u002Fionicons\u002F2.0.1\u002Fcss\u002Fionicons.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002Ftempusdominus-bootstrap-4\u002Fcss\u002Ftempusdominus-bootstrap-4.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002Ficheck-bootstrap\u002Ficheck-bootstrap.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002Fjqvmap\u002Fjqvmap.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fdist\u002Fcss\u002Fadminlte.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002FoverlayScrollbars\u002Fcss\u002FOverlayScrollbars.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002Fdaterangepicker\u002Fdaterangepicker.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002Fsummernote\u002Fsummernote-bs4.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"\u002Fplugins\u002Fsweetalert2-theme-bootstrap-4\u002Fbootstrap-4.min.css","type":"text\u002Fcss"},{"rel":"stylesheet","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss?family=Source+Sans+Pro:300,400,400i,700","type":"text\u002Fcss"},{"rel":"stylesheet","href":"","type":"text\u002Fcss"},{"rel":"stylesheet","href":"","type":"text\u002Fcss"}],"script":[{"src":"\u002Fplugins\u002Fjquery\u002Fjquery.min.js","body":true},{"src":"\u002Fplugins\u002Fjquery-ui\u002Fjquery-ui.min.js","body":true},{"src":"\u002Fplugins\u002Fbootstrap\u002Fjs\u002Fbootstrap.bundle.min.js","body":true},{"src":"\u002Fplugins\u002Fsweetalert2\u002Fsweetalert2.min.js","body":true},{"src":"\u002Fplugins\u002Fsummernote\u002Fsummernote-bs4.min.js","body":true},{"src":"\u002Fplugins\u002Fchart.js\u002FChart.min.js","body":true},{"src":"\u002Fplugins\u002Fsparklines\u002Fsparkline.js","body":true},{"src":"\u002Fplugins\u002Fjqvmap\u002Fjquery.vmap.min.js","body":true},{"src":"\u002Fplugins\u002Fjqvmap\u002Fmaps\u002Fjquery.vmap.usa.js","body":true},{"src":"\u002Fplugins\u002Fjquery-knob\u002Fjquery.knob.min.js","body":true},{"src":"\u002Fplugins\u002Fmoment\u002Fmoment.min.js","body":true},{"src":"\u002Fplugins\u002Fdaterangepicker\u002Fdaterangepicker.js","body":true},{"src":"\u002Fplugins\u002Ftempusdominus-bootstrap-4\u002Fjs\u002Ftempusdominus-bootstrap-4.min.js","body":true},{"src":"\u002Fplugins\u002FoverlayScrollbars\u002Fjs\u002Fjquery.overlayScrollbars.min.js","body":true},{"src":"\u002Fdist\u002Fjs\u002Fadminlte.js","body":true},{"src":"\u002Fdist\u002Fjs\u002Fdemo.js","body":true}],"style":[]},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  function inject(key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value
    // Add into context
    if (!app.context[key]) {
      app.context[key] = value
    }

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue.prototype, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  // Inject runtime config as $config
  inject('config', config)

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }

  // Add enablePreview(previewData = {}) in context for plugins
  if (process.static && process.client) {
    app.context.enablePreview = function (previewData = {}) {
      app.previewData = Object.assign({}, previewData)
      inject('preview', previewData)
    }
  }
  // Plugin execution

  if (typeof nuxt_plugin_bootstrapvue_b081ad28 === 'function') {
    await nuxt_plugin_bootstrapvue_b081ad28(app.context, inject)
  }

  if (typeof nuxt_plugin_vcurrencyfield_4fcff766 === 'function') {
    await nuxt_plugin_vcurrencyfield_4fcff766(app.context, inject)
  }

  if (typeof nuxt_plugin_nuxtgooglemaps_5ab8847a === 'function') {
    await nuxt_plugin_nuxtgooglemaps_5ab8847a(app.context, inject)
  }

  if (typeof nuxt_plugin_VueResource_4bf21b44 === 'function') {
    await nuxt_plugin_VueResource_4bf21b44(app.context, inject)
  }

  if (typeof nuxt_plugin_VueMoment_21420450 === 'function') {
    await nuxt_plugin_VueMoment_21420450(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_localStorage_830ec59e === 'function') {
    await nuxt_plugin_localStorage_830ec59e(app.context, inject)
  }

  // Lock enablePreview in context
  if (process.static && process.client) {
    app.context.enablePreview = function () {
      console.warn('You cannot call enablePreview() outside a plugin.')
    }
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, (err) => {
        // https://github.com/vuejs/vue-router/blob/v3.4.3/src/util/errors.js
        if (!err._isRouter) return reject(err)
        if (err.type !== 2 /* NavigationFailureType.redirected */) return resolve()

        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
