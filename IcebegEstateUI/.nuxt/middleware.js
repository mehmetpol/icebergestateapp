const middleware = {}

middleware['admin'] = require('..\\middleware\\admin.js')
middleware['admin'] = middleware['admin'].default || middleware['admin']

middleware['auth'] = require('..\\middleware\\auth.js')
middleware['auth'] = middleware['auth'].default || middleware['auth']

middleware['session-control'] = require('..\\middleware\\session-control.js')
middleware['session-control'] = middleware['session-control'].default || middleware['session-control']

middleware['signin'] = require('..\\middleware\\signin.js')
middleware['signin'] = middleware['signin'].default || middleware['signin']

export default middleware
