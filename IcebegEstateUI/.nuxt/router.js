import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _6a1150ac = () => interopDefault(import('..\\pages\\Login.vue' /* webpackChunkName: "pages/Login" */))
const _6df1f860 = () => interopDefault(import('..\\pages\\Map.js' /* webpackChunkName: "pages/Map" */))
const _d81433ba = () => interopDefault(import('..\\pages\\New.vue' /* webpackChunkName: "pages/New" */))
const _219c5f08 = () => interopDefault(import('..\\pages\\Appointment\\_id.vue' /* webpackChunkName: "pages/Appointment/_id" */))
const _28f78db5 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/Login",
    component: _6a1150ac,
    name: "Login"
  }, {
    path: "/Map",
    component: _6df1f860,
    name: "Map"
  }, {
    path: "/New",
    component: _d81433ba,
    name: "New"
  }, {
    path: "/Appointment/:id?",
    component: _219c5f08,
    name: "Appointment-id"
  }, {
    path: "/",
    component: _28f78db5,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
