import * as tr from "./static/plugins/fullcalendar/locales/tr";

export default {


    server: {
        port: 5656
    },
    mode: "universal",
    head: {

        title: "Iceberg Estate",
        meta: [
            { charset: 'utf-8' },
            { lang: "en" },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            //   { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=no' },

            { hid: 'description', name: 'description', content: "Iceber Estate" }
        ],
        htmlAttrs: {
            lang: "tr",

        },
        link: [
    
            { rel: 'icon', type: 'image/png', href: '/img/fav.png' },
            { rel: "stylesheet", href: "/css/uygulama.css", type: "text/css" },
            { rel: "stylesheet", href: "/css/ScrollBar.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/fontawesome-free/css/all.min.css", type: "text/css" },
            { rel: "stylesheet", href: "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/icheck-bootstrap/icheck-bootstrap.min.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/jqvmap/jqvmap.min.css", type: "text/css" },
            { rel: "stylesheet", href: "/dist/css/adminlte.min.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/overlayScrollbars/css/OverlayScrollbars.min.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/daterangepicker/daterangepicker.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/summernote/summernote-bs4.css", type: "text/css" },
            { rel: "stylesheet", href: "/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css", type: "text/css" },
            { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700", type: "text/css" },
            { rel: "stylesheet", href: "", type: "text/css" },
            { rel: "stylesheet", href: "", type: "text/css" },
        ],

        script: [
            { src: "/plugins/jquery/jquery.min.js", body: true },
            { src: "/plugins/jquery-ui/jquery-ui.min.js", body: true },
            { src: "/plugins/bootstrap/js/bootstrap.bundle.min.js", body: true },
            { src: "/plugins/sweetalert2/sweetalert2.min.js", body: true },
            { src: "/plugins/summernote/summernote-bs4.min.js", body: true },

            { src: "/plugins/chart.js/Chart.min.js", body: true },
            { src: "/plugins/sparklines/sparkline.js", body: true },
            { src: "/plugins/jqvmap/jquery.vmap.min.js", body: true },
            { src: "/plugins/jqvmap/maps/jquery.vmap.usa.js", body: true },
            { src: "/plugins/jquery-knob/jquery.knob.min.js", body: true },
            { src: "/plugins/moment/moment.min.js", body: true },
            { src: "/plugins/daterangepicker/daterangepicker.js", body: true },
            { src: "/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js", body: true },
            { src: "/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js", body: true },
            { src: "/dist/js/adminlte.js", body: true },
            { src: "/dist/js/demo.js", body: true },

        ]
    },

    loading: { color: '#af1116' },

    css: [],



    plugins: [
        { src: '~/plugins/VueResource' },
        { src: '~/plugins/VueMoment' },
        { src: '~/plugins/localStorage.js', ssr: false },
        
        

    ],
    transition: {
        name: "layout",
        mode: "out-in"
    },

    bootstrapVue: {
        icons: true
    },
    modules: [
        ['bootstrap-vue/nuxt'],

        ['nuxt-gmaps', {
            key: 'AIzaSyDV29qqPy2su7Ge274vY1ed4ywdgciQ9Vk',
            libraries: ["geometry", "places"],
        }],

        ['v-currency-field/nuxt', {
            locale: 'tr-TR',
            decimalLength: 0,
            autoDecimalMode: false,
            min: null,
            max: null,
            defaultValue: null,
            valueAsInteger: false,
            allowNegative: false,
            distractionFree: false,
        }],
    ],
    build: {
        extend(config, ctx) {
        }
    },
    buildModules: []
};
