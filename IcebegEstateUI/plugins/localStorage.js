import createPersistedState from 'vuex-persistedstate';
import SecureLS from "secure-ls";
const ls = new SecureLS({ isCompression: false });
import * as Cookies from 'js-cookie';
import cookie from 'cookie';

export default ({ store, req, isDev }) => {
    window.onNuxtReady(() => {

        createPersistedState({
            key: 'c27d35',
            paths: [],
            storage: {
                getItem: key => ls.get(key),
                //getItem: (key) => process.client ? ls.get(key) : cookie.parse(req.headers.cookie || '')[key],
                setItem: (key, value) => ls.set(key, value),
                removeItem: key => ls.remove(key)
            }
        })(store);

    });    
}