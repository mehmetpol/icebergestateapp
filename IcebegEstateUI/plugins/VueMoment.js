import Vue from "vue";
const moment = require('moment');
import numeral from "numeral";
import nutr from 'numeral/locales/tr';

import { ValidationProvider, ValidationObserver } from 'vee-validate/dist/vee-validate.full';

Vue.component("ValidationProvider",ValidationProvider);
Vue.component("ValidationObserver",ValidationObserver);



import VueMask from "vue-the-mask";
Vue.use(VueMask);

require('moment/locale/tr');
Vue.use(require('vue-moment'), {
    moment
});



numeral.locale('tr');

Vue.filter('ikihane', function (value) {
    if (!value) return '0,00';
    return  numeral(value).format("0,0.00");
});

let Mix = {
    methods: {
        SayiFormatla: function (Dgr,Hane) {
            
           
            if (Dgr > 0) {
                if (Hane === 2) {
                    return numeral(Dgr).format("0,0.00");
                }
                if (Hane===4) {
                    return numeral(Dgr).format("0,0.0000");
                }
                if (Hane===0) {
                    return numeral(Dgr).format("0,0");
                }
                
            }
            return "";
        },


        MesajFn: function (Cevap) {
            this.$root.$confirm(Cevap.Durum ? 'İşlem Başarılı' : 'Hata', Cevap.Mesaj, { color: Cevap.Durum ? 'success' : 'error' }).then((confirm) => {
            });
        },
        blobToDataURL: function (blob, callback) {
            var a = new FileReader();
            a.onload = function (e) { callback(e.target.result); };
            a.readAsDataURL(blob);
        }
    }
};
Vue.mixin(Mix);
