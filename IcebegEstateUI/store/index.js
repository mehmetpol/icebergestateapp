import Vue from "vue";
const moment = require('moment');


export const state = () => ({
    Kb: null,
    ApiLink: "http://api.codenetyazilim.com/",
    Yukleniyor: false,
    MobilBilgi: { dv: null, dt: null, ck: null },
    AppointmentList: [],    
    OfficePosition: { lat: 37.027811, lng: 37.306735 },
});
export const getters = {

    getAppointmentList(state) {

        return state.AppointmentList;
    },    
    getKb(state) {
        if (state.Kb) {
            return state.Kb;
        }
        return null;
    },
    isAdmin(state) {
        if (state.Kb) {
            return state.Kb.Rolleri.includes("ADMIN");
        }
        return false;
    },
};
export const mutations = {
    setClearAppointment(state) {
        state.AppointmentList = [];
    },
    setAppointment(state, data) {
        
        var k = state.AppointmentList.find(x => x.Id === data.Id);
        if (k) {
            k.Address = data.Address;
            k.Date = data.Date;
            k.Name = data.Name;
            k.LastName = data.LastName;
            k.Email = data.Email;
            k.Telephone = data.Telephone;
            k.Start = data.Start;
            k.OutDate = data.OutDate;
            k.ReturnDate = data.ReturnDate;
            k.Hours = data.Hours;
            k.Start = data.Start;
            k.Destination = data.Destination;                        
            k.Distance = data.Distance;
            k.Duration = data.Duration;
            k.DestinationAddsress = data.DestinationAddsress;
            k.OriginAddress = data.OriginAddress;
            k.NameLastName = data.NameLastName;

        }
        else {
            state.AppointmentList.push(data);
        }
    },
    setKb(state, kb) {

        if (kb && kb.length > 0) {
            state.Kb = kb[0];
            let Menu = kb[1];
            state.Menu = Menu;
        }
        else {
            state.Kb = null;
            state.Menu = [];
        }
    },

};

export const actions = {
    requestPost(vuexContext, Gonder) {
        let Token = vuexContext.getters["auth/getToken"];

        Token = "Bearer " + Token;
        Gonder.Link = vuexContext.state.ApiLink + Gonder.Link;
        return Vue.http.post(Gonder.Link, Gonder.Data, { headers: { "Authorization": Token } })
            .catch((r) => {
                

                if (r && r.status && r.status === 401) {
                    console.log("requestPost auth/logout");
                    vuexContext.dispatch("auth/logout");
                    if (process.client) {

                        var url = "/Login";
                        var mb = vuexContext.state.MobilBilgi;
                        if (mb.dv) {
                            url = "/Login?dv=" + mb.dv + "&dt=" + mb.dt + "&ck=" + mb.ck;
                        }
                        this.app.router.push(url);
                    }

                } else { console.log("Hata", r); }
            });
    },
    requestGet(vuexContext, link) {

        let Token = vuexContext.getters["auth/getToken"];
        Token = "Bearer " + Token;
        link = vuexContext.state.ApiLink + link;
        return Vue.http.get(link, { headers: { "Authorization": Token } })
            .catch((r) => {
                
                if (r && r.status && r.status === 401) {

                    vuexContext.dispatch("auth/logout");
                    if (process.client) {
                        var url = "/Login";
                        var mb = vuexContext.state.MobilBilgi;
                        if (mb.dv) {
                            url = "/Login?dv=" + mb.dv + "&dt=" + mb.dt + "&ck=" + mb.ck;
                        }
                        this.app.router.push(url);
                    }
                } else { console.log("Hata", r); }
            });
    },
    requestKb({ state, commit, rootState, dispatch }, tkn) {
        let Token = tkn.token;
        Token = "Bearer " + Token;
        var ths = this;
        var link = rootState.ApiLink + "api/Kullanicilar/KbGetir";
        Vue.http.get(link, { headers: { "Authorization": Token } }).then(function (res) {
            var Cevap = res.body;
            if (Cevap.Durum) {
                commit("setKb", Cevap.Veri);
                if (state.ReturnUrl) {
                    ths.app.router.push(state.ReturnUrl);
                }
                else {
                    ths.app.router.push("/");
                }
                
            }
        }).catch((r) => {
            
            console.log("Kb hata", r);
        });
    },

};



